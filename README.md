# project1

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

Task :
- Part 1, testing basic vue di project tsb 
- Part 2, routing & vuex, bikin lebih dari 1 halaman, about-us misal 
- Part 3, form & validaton, bikin halaman dengan form contact us 
- Part 4, module, header & footer di semua halaman dijadikan komponen sendiri 

Done :
Part 1, Part 2 Routing

To Do :
Part 2 Vuex, Part 3 Form & Validation