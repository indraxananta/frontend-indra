import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
	state: {
		counter: 0,
		isLoading: false,
		listCompany: [
			{ companyName: 'MTARGET 1', companyType: 'PT' },
			{ companyName: 'MTARGET 2', companyType: 'PT' },
			{ companyName: 'MTARGET 3', companyType: 'PT' }
		]		
	},
	getters: {
		doubleCounter: state => {
			return state.counter * 2;
		},
		stringCounter: state => {
			return state.counter + ' Clicks';
		}
	},
	mutations: {
		increment: (state, payload) => {
			state.counter += payload;
		},
		decrement: (state, payload) => {
			state.counter -= payload;
		},
		KONFIRMASI_COMPANY: (state, company) => {
			state.listCompany.push(company);
		}
	},
	actions: {
		increment: ({ commit }, payload) => {
			commit('increment', payload);
		},
		decrement: ({ commit }, payload) => {
			commit('decrement', payload);
		},
		asyncIncrement: ({commit}) => {
			setTimeout(() => {
				commit('increment');
			}, 1000);
		},
		asyncDecrement: ({commit}) => {
			setTimeout(() => {
				commit('decrement');
			}, 1000);
		},
		simpanCompany({ commit, state }, company) {
			state.isLoading = true;
			setTimeout(() => {
				commit('KONFIRMASI_COMPANY', company);
				state.isLoading = false;
			}, 1500);
		}
	}
});