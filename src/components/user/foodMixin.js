export const foodMixin = {
	data() {
		return {
			foods: ['Rice', 'Noodle', 'Biscuits'],
			filterText: ''
		};
	},
	computed: {
		filteredFoods() {
			return this.foods.filter((element) => {
				return element.match(this.filterText);
			});
		}
	},
	created() {
		console.log('Created');
	}
};