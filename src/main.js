import Vue from 'vue';
import VueRouter from 'vue-router';
import { store } from './store/store';
import App from './App.vue';
import {routes} from './routes';
import Vuelidate from 'vuelidate';

Vue.use(VueRouter);
Vue.use(Vuelidate);
Vue.filter('to-lowercase');

const router = new VueRouter({
	routes,
	mode: 'history'
});

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app');
