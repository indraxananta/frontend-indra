import Home from './components/user/Home.vue';
import AboutUs from './components/user/AboutUs.vue';
import Product from './components/user/Product.vue';
import ListProduct from './components/user/ListProduct.vue';
import DetailProduct from './components/user/DetailProduct.vue';
import EditProduct from './components/user/EditProduct.vue';
import Contact from './components/user/Contact.vue';
import VuexPage from './components/user/VuexPage.vue';
import VueAxios from './components/user/VueAxios.vue';
import FetchApi from './components/user/FetchApi.vue';

export const routes = [
	{path: '', component: Home},
	{path: '/aboutus', component: AboutUs},
	{path: '/product', component: Product, 
		children: [
			{path: '', component: ListProduct},
			{path: ':id', component: DetailProduct},
			{path: ':id/edit', component: EditProduct, name: 'editProduct'}
		]
	},
	{path: '/contact', component: Contact},
	{path: '/vuexpage', component: VuexPage},
	{path: '/vueaxios', component: VueAxios},
	{path: '/fetchapi', component: FetchApi},
];